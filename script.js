// console.log("Hello World! This is from External JS");


// single line  CMD + /
// multi-line comment CMD + SHIFT + /



/*
	Syntax and Statement 

	Syntax

		- set of rules of how codes should be writted correctly


	Statement
		-set of instructions. ends with semicolon



*/

// alert ("Good afternoon");


/*
	Variables
		- a container that holds value


*/

// let myName = "Roxanne";
// console.log(myName);


/* Anatomy

	let
		- declaration
		- declaring a variable
		-cannot re-declare same variable name under same scope


		Ex. 
			let car;
			console.log(car);

			let car = "mercedes";
			console.log(car);


	** Initialization
		initializing a variable with a value


		Ex. 
			let phone = "Iphone";
			console.log(phone);



	** Re-assignment
		assigning new value to a variable using equal operator


		Ex. myName = "Gab";
			console.log(myName);






	// Why is variable important? 
		// - reusability



	console.log(lasName);
		// without let keyword, return will be "NOT DEFINED"


	let firstName;
	console.log(firstName);
		// returns "UNDEFINED"




/*
	Naming Convention


	1. Case sensitive

		let color = "pink";
		let Color = "blue";


		console.log(color);
		console.log(Color);



	2. Variable name must be relevant to its actual value

		let value = "Robin";

		let goodThief = "Robin";
		let bird = "Robin";



	3. Camel Case Notation 
		- 2nd word of is capital

		let capitalcityofthephilippines = "Manila City";

		let capitalCityOfThePhilippines = "Manila City";






******* @ & numbers are not accepted on declaration
******* Single and double quotes are accepted


*/


// ES6 updates
	// template literals - using backticks 


// console.log(She said "Hello there")



/*
	Constant
		- a type of variable that holds va


		const PI = 3.14;
		console.log(PI);

		PI = 14
		console.log(PI);
			- reassignment is not allowed


		const boilingPoint = 100;
		console.log(boilingPoint);
			- declaration must have value else it will be errror



*/




// let country = "Philippines";
// console.log(country);

// let continent = "Asia";
// console.log(continent);

// let population = "109 million";
// console.log(population);







/*

	Data Types



		1. String
			- sequence of characters
			- always wrapped with quotes or backticks
			- if no quotes backticks, JS will think of it as another variable
			- black in console


			let food = "sinigang";
			let name = "Roxanne"




		2. Number
			- hole number (integer) & decimals (float)
			- blue in console


			let x = 4;
			let y = 8;

			Let result = x + y;
			console.log(result);




		3. Boolean
			- values TRUE or FALSE
			- blue in console

			
			let isEarly = true;
			console.log(isEarly);
			let areLate = false;
			console.log(areLate);



		4. Undefined
			- varable has been declared however no value yet
			- empty value

			
			let job;
			console.log(job);



		5. Null
			- empty value
			- it is used to represent an empty value



		6. BigInt()
			- large integers more than the data type can hold



		7. Object
			- one variable can handle or contain several types of  data
			- wrapped with curly braces
			- has property and value


	



	// **special type of object
			// Array

			 - collection of related data
			 - enclosed with square brackets

		let fruits = ["apple", "banna", "strawberry"];
		let grades = [89, 90, 92, 97, 95];



	type of Operator 
		- evaluates what type of data is that you are working with
		- "typeOf"


*/















































